package cn.atuyang.work.bigdata0725.hbase.client;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.NamespaceDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author aTuYang
 */
public class AppMain {

    public static void main(String[] args) throws IOException {

        try {
            Configuration configuration = HBaseConfiguration.create();
            configuration.set("hbase.zookeeper.quorum", "jikehadoop01");
            configuration.set("hbase.zookeeper.property.clientPort", "2181");
            Connection connection = ConnectionFactory.createConnection(configuration);
            Admin hbaseAdmin = connection.getAdmin();

            // 判断是否存在namespace，存在则删除重新创建
            final NamespaceDescriptor yangxunyao = hbaseAdmin.getNamespaceDescriptor("yangxunyao");
            if (yangxunyao != null) {
                final TableName[] yangxunyaos = hbaseAdmin.listTableNamesByNamespace("yangxunyao");
                for (TableName tableName : yangxunyaos) {
                    hbaseAdmin.disableTable(tableName);
                    hbaseAdmin.deleteTable(tableName);
                }
                hbaseAdmin.deleteNamespace("yangxunyao");
            }
            hbaseAdmin.createNamespace(NamespaceDescriptor.create("yangxunyao").build());

            // 创建表yangxunyao:student
            List<ColumnFamilyDescriptor> columnFamilyDescriptors = new ArrayList<>();
            columnFamilyDescriptors.add(ColumnFamilyDescriptorBuilder.of("name"));
            columnFamilyDescriptors.add(ColumnFamilyDescriptorBuilder.of("info"));
            columnFamilyDescriptors.add(ColumnFamilyDescriptorBuilder.of("score"));
            final TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder
                    .newBuilder(TableName.valueOf("yangxunyao:student"))
                    .setColumnFamilies(columnFamilyDescriptors);
            hbaseAdmin.createTable(tableDescriptorBuilder.build());

            // 插入数据
            final Table table = hbaseAdmin.getConnection().getTable(TableName.valueOf("yangxunyao:student"));
            final byte[] row = UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8);
            Put put = new Put(row);
            put.addColumn("name".getBytes(StandardCharsets.UTF_8)
                    , "".getBytes(StandardCharsets.UTF_8)
                    , "杨训垚".getBytes(StandardCharsets.UTF_8));
            put.addColumn("info".getBytes(StandardCharsets.UTF_8)
                    , "student_id".getBytes(StandardCharsets.UTF_8)
                    , "G20210735010232".getBytes(StandardCharsets.UTF_8));
            put.addColumn("info".getBytes(StandardCharsets.UTF_8)
                    , "class".getBytes(StandardCharsets.UTF_8)
                    , "2".getBytes(StandardCharsets.UTF_8));
            table.put(put);
            table.close();

            // 查询数据
            ResultScanner scanner = table.getScanner(new Scan());
            List<Map<String, String>> list = new ArrayList<>();
            for(Result result : scanner) {
                HashMap<String, String> map = new HashMap<>();
                //rowkey
                String rowkey = Bytes.toString(result.getRow());
                map.put("row", rowkey);
                for (Cell cell : result.listCells()) {
                    //列族
                    String family = Bytes.toString(cell.getFamilyArray(),
                            cell.getFamilyOffset(), cell.getFamilyLength());
                    //列
                    String qualifier = Bytes.toString(cell.getQualifierArray(),
                            cell.getQualifierOffset(), cell.getQualifierLength());
                    //值
                    String data = Bytes.toString(cell.getValueArray(),
                            cell.getValueOffset(), cell.getValueLength());
                    map.put(family + ":" + qualifier, data);
                }
                list.add(map);
            }
            System.out.println(list);

            // 删除数据
            table.delete(new Delete(row));
            final Result result = table.get(new Get(row));
            if (result != null) {
                System.out.println("查无数据。");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
